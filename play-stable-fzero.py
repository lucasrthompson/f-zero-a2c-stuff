import retro

from stable_baselines.common.policies import MlpPolicy, MlpLstmPolicy, MlpLnLstmPolicy, CnnLnLstmPolicy, CnnPolicy, CnnLstmPolicy
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines import PPO2, A2C
import numpy as np
import gym
import retrowrapper


class Discretizer(gym.ActionWrapper):
    """
    Wrap a gym-retro environment and make it use discrete
    actions for the Sonic game.
    """
    def __init__(self, env):
        super(Discretizer, self).__init__(env)
        #[1,0,0,0,0,0,0,0,0,0,0,0],[1,0,0,0,0,0,0,1,0,0,0,0],[1,0,0,0,0,0,1,0,0,0,0,0]]
        buttons = ["B", "A", "SELECT", "START", "UP", "DOWN", "LEFT", "RIGHT", "C", "Y", "X", "Z"] #not correct yet but good enough for FZero
        actions = [['B'], ['LEFT', 'B'], ['RIGHT', 'B']]
        self._actions = []
        for action in actions:
            arr = np.array([False] * 12)
            for button in action:
                arr[buttons.index(button)] = True
            self._actions.append(arr)
        self.action_space = gym.spaces.Discrete(len(self._actions))
        #print(self.action_space)

    def action(self, a): # pylint: disable=W0221
        #print(self._actions[a].copy())
        return self._actions[a].copy()

sts = ['GP1',
        'BBP1',
        'SOP1',
        'DWP1',
        'SP1',
        'WLP1',
        'PLP1', 'go']
n_cpu = 5

modelname = ("a2c-fzero")
model = A2C.load(modelname)

env = SubprocVecEnv([lambda: Discretizer(retro.make('FZero-Snes', state='go', scenario='training')) for i in range(n_cpu)])
#model = A2C(CnnPolicy, env, n_steps=128, verbose=1)   
model.set_env(env)

#model.learn(total_timesteps=10000000)
#model.save(modelname)
#env.close()
#for st in sts:
    #print(st)
    #env = SubprocVecEnv([lambda: Discretizer(retro.make('FZero-Snes', state=st, scenario='training')) for i in range(n_cpu)])    
    #model.set_env(env)
    ##model = PPO2.load(modelname)
    #model.learn(total_timesteps=100000)
    #model.save(modelname)
    #env.close()

#env = SubprocVecEnv([lambda: Discretizer(retro.make('FZero-Snes', state=sts[i], scenario='training')) for i in range(len(sts))])    
#model.set_env(env)
#del model # remove to demonstrate saving and loading
#model = PPO2.load("ppo2-FZER-GO-cnn")

input("Hit Enter")
#env = SubprocVecEnv([lambda: Discretizer(retro.make('FZero-Snes', state='go', scenario='scenario')) for i in range(n_cpu)])
for st in sts:
    env = Discretizer(retro.make('FZero-Snes', state=st, record='.'))
    #model.set_env(env)
    #env = SubprocVecEnv([lambda: env1 for i in range(n_cpu)])
    dones = False
    obs = env.reset()
    print(obs.shape)
    counter = 0
    while not dones:
        action, _states = model.predict(obs)

        obs, rewards, dones, info = env.step(action)
        if rewards > 0:
            counter = 0
        else:
            counter += 1
                
        if counter > 250:
            dones = True
        #print(dones)
        env.render()
        if dones:
        #if dones.all() == True:
            obs = env.reset()
    env.close()


